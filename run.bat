@echo off
WHERE pytshon >nul 2>nul
IF %ERRORLEVEL% EQU 0 python QrCodeGenerator.pyw
IF %ERRORLEVEL% NEQ 0 ECHO ========= & ECHO.!WARNING! & ECHO.========= & ECHO. & ECHO.Python was not found on this system. Visit http://python.org/ to download the & ECHO.latest version (3.0+) or check that it's present in your PATH variable. & ECHO.
PAUSE
EXIT 0
