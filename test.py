import sys
import qrcode as qr
from PyQt4 import QtGui
from PIL import ImageQt

a = QtGui.QApplication(sys.argv)
l = QtGui.QLabel()
qrimg = qr.make("Some test data")
qrimg.save('test2.png')
pix = QtGui.QPixmap.fromImage(ImageQt.ImageQt(qrimg))
l.setPixmap(pix)
l.show()
sys.exit(a.exec_())
