import os
import sys
import qrcode as qr

from PyQt4 import QtGui, QtCore
from main_window import Ui_main_window as Ui
import ui.icons as ico


def b64_to_image(base64_encoded_image: str, img_type='pixmap'):
    if img_type == 'pixmap':
        return QtGui.QPixmap.fromImage(QtGui.QImage.fromData(QtCore.QByteArray.fromBase64(base64_encoded_image), "PNG"))
    elif img_type == 'image':
        return QtGui.QImage.fromData(QtCore.QByteArray.fromBase64(base64_encoded_image), "PNG")
    elif img_type == 'icon':
        return QtGui.QIcon(QtGui.QPixmap.fromImage(QtGui.QImage.fromData(QtCore.QByteArray.fromBase64(base64_encoded_image), "PNG")))


class QRCodeGenerator(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.ui = Ui()
        self.ui.setupUi(self)
        self.setWindowIcon(b64_to_image(ico.window_icon, 'icon'))
        self.ui.tab_chooser.setTabIcon(0, b64_to_image(ico.link, 'icon'))
        self.ui.tab_chooser.setTabIcon(1, b64_to_image(ico.text, 'icon'))
        self.ui.tab_chooser.setTabIcon(2, b64_to_image(ico.wifi, 'icon'))
        self.ui.tab_chooser.setTabIcon(3, b64_to_image(ico.mecard, 'icon'))

        self.filename = ""
        self.current_qr_code = QtGui.QImage()

        self.ui.btn_quit.setIcon(b64_to_image(ico.quit, 'icon'))
        self.ui.btn_quit.clicked.connect(self.close)
        self.ui.btn_browse.setIcon(b64_to_image(ico.browse, 'icon'))
        self.ui.btn_browse.clicked.connect(self.set_image_filename)
        self.ui.btn_generate_qr.setIcon(b64_to_image(ico.generate, 'icon'))
        self.ui.btn_generate_qr.clicked.connect(self.select_qr_type)
        self.ui.btn_save.setIcon(b64_to_image(ico.save, 'icon'))
        self.ui.btn_save.clicked.connect(self.save_to_file)
        self.ui.flag_pw_visible.clicked.connect(self.toggle_pw_visibility)

        self.ui.txt_simple_text.textChanged.connect(self.update_char_count)

    def set_image_filename(self):
        fname = QtGui.QFileDialog().getSaveFileName(self, "Select base folder", os.getcwd())
        if fname != "":
            self.ui.txt_filename.setText(fname)

    def generate_url(self):
        return self.ui.txt_url.text()

    def generate_text(self):
        return self.ui.txt_simple_text.toPlainText()

    def generate_wifi(self):
        wifi_psw_strength = "nopass" if self.ui.combo_wifi_security.currentText() == "No password" else \
            self.ui.combo_wifi_security.currentText()
        ssid = self.ui.txt_ssid.text()
        password = self.ui.txt_password.text()
        hidden = "TRUE" if self.ui.combo_hidden.currentText() == "Yes" else "FALSE"
        data = "WIFI:T:%s;S:%s;P:%s;H:%s;" % (wifi_psw_strength, ssid, password, hidden)
        print(data)
        return data

    def generate_mecard(self):
        mecard = "MECARD:"
        n = ""
        adr = ""
        # Name construction
        if self.ui.txt_mecard_surname.text() != "":
            n = "%s" % self.ui.txt_mecard_surname.text()
        if self.ui.txt_mecard_name.text() != "":
            n += ",%s" % self.ui.txt_mecard_name.text()
        mecard += "N:" + n + ";"
        # Address construction
        if self.ui.txt_mecard_email.text() != "":
            mecard += "EMAIL:%s;" % self.ui.txt_mecard_email.text()
        if self.ui.txt_mecard_phone.text() != "":
            mecard += "TEL:%s;" % self.ui.txt_mecard_phone.text()
        if self.ui.txt_mecard_street.text() != "":
            adr += self.ui.txt_mecard_street.text()
        if self.ui.txt_mecard_city.text() != "":
            adr += ", " + self.ui.txt_mecard_city.text()
        if self.ui.txt_mecard_zip.text() != "":
            adr += ", " + self.ui.txt_mecard_zip.text()
        if self.ui.txt_mecard_country.text() != "":
            adr += " " + self.ui.txt_mecard_country.text()
        mecard += "ADR:" + adr + ";"
        # Url insertion
        if self.ui.txt_mecard_url.text() != "":
            mecard += "URL:http://" + self.ui.txt_mecard_url.text()
        mecard += ";;"
        print(mecard)
        return mecard

    def update_char_count(self):
        present_chars = len(self.ui.txt_simple_text.toPlainText())
        char_left = 3000 - present_chars
        if char_left < 0:
            self.ui.lbl_simple_text.setStyleSheet("QLabel {color: red;}")
        else:
            self.ui.lbl_simple_text.setStyleSheet("QLabel {color: black;}")
        self.ui.lbl_simple_text.setText("Plain text to encode in the QR image (%d characters left)" % char_left)

    def select_qr_type(self):
        tab = self.ui.tab_chooser.currentIndex()
        if tab == 0:
            data = self.generate_url()
        elif tab == 1:
            data = self.generate_text()
        elif tab == 2:
            data = self.generate_wifi()
        elif tab == 3:
            data = self.generate_mecard()
        else:
            data = "NO DATA ENCODED"
        if len(data) > 3000:
            box = QtGui.QMessageBox(self)
            box.setWindowTitle("Error")
            box.setText("Text can't be more than 3000 characters.")
            box.setModal(True)
            box.setIconPixmap(b64_to_image(ico.error))
            box.show()
        else:
            qr.make(data).save('tmp.png')
            self.ui.preview.setPixmap(QtGui.QPixmap.fromImage(QtGui.QImage('tmp.png')))
            h = self.ui.preview.pixmap().height()
            w = self.ui.preview.pixmap().width()
            self.ui.lbl_width.setText(str(w))
            self.ui.lbl_height.setText(str(h))
            os.remove('tmp.png')

    def save_to_file(self):
        filename = self.ui.txt_filename.text()
        if filename == '' or (not os.path.isdir(os.path.split(filename)[0])):
            print("Can't save on invalid path.")
        elif self.ui.preview.pixmap() == 0:
            print("There is no generated qr code to save")
        else:
            self.ui.preview.pixmap().save(self.ui.txt_filename.text())

    def toggle_pw_visibility(self):
        if self.ui.flag_pw_visible.checkState():
            self.ui.txt_password.setEchoMode(QtGui.QLineEdit.Normal)
        else:
            self.ui.txt_password.setEchoMode(QtGui.QLineEdit.PasswordEchoOnEdit)


if __name__ == '__main__':
    import ctypes
    myappid = "marcellomassaro.qrcodegenerator"
    ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)
    app = QtGui.QApplication(sys.argv)
    win = QRCodeGenerator()
    win.show()
    sys.exit(app.exec_())
