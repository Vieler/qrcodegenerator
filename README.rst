===============
QrCodeGenerator
===============
--------------------------------------------------
A simple GUI for writing your QR codes on the fly!
--------------------------------------------------

Introduction
============
**QrCodeGenerator** (QCG for short) is a Python_ (3.0+) program, written with the PyQt4_ GUI Toolkit. This is a very simple program
capable of producing **2D QR Codes V2** (that is, version 2) using the standard python library qrcode_.

Features
--------
As of now, **QCG** is capable of encoding the following data in the appropriate QR Code:

* **URLs** - Embed any kind of URL (or URI) to a QR Code to later distribute
* **Plain text** - Simple plain text (up to 3000 characters, to be useful) to share short blocks of text
* **WIFI settings** - Using thi QR Code, you'll be ablo to quickly share/connect to a network
* **Contact info** - This is perfect if you want to embed your contact information into physical media (business cards, flyers, etc...)

For now, files can only be saved into **PNG** format. Support for **SVG** image files will be added.


.. _Python: http://python.org/
.. _PyQt4: https://riverbankcomputing.com/software/pyqt/intro
.. _qrcode: https://pypi.python.org/pypi/qrcode